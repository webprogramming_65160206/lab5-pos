import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
    const members = ref<Member[]>([
        { id: 1, name: 'มานะ รักชาติ', tel: '0881234567' },
        { id: 2, name: 'มาดี มีใจ', tel: '0881234567' }

    ])
    const currentMember = ref<Member | null>()
    // behind the parameter in of function is what it will return
    const searchMember = (tel: string) => {
        const index = members.value.findIndex((item) => item.tel === tel)
        if (index < 0) {
            currentMember.value = null
        }
        currentMember.value = members.value[index]
    }
    const clear = () => {
        currentMember.value = null
    }
    return {
        members, currentMember,
        searchMember, clear
    }
})
